package com.example.changecolor;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import java.util.Random;
import androidx.appcompat.app.AppCompatActivity;
public class MainActivity extends AppCompatActivity {

    View screenView;
    Button ChangeColor;
    int[] color;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        color = new int[] {Color.BLACK, Color.BLUE, Color.GRAY, Color.GREEN, Color.YELLOW, Color.CYAN};
        screenView =  findViewById(R.id.rView);
        ChangeColor = findViewById(R.id.button);

        ChangeColor.setOnClickListener(view -> {
            int aryLength = color.length;

            Random random = new Random();
            int rNum = random.nextInt(aryLength);

            screenView.setBackgroundColor(color[rNum]);
        });
    }
}

